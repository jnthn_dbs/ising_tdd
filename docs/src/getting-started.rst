Getting Started
===============

First Run
---------

The following descriptions assume that the Arduino based setup is used. Any other hardware platform works analogous. 

To get the project up and running, perform the following steps:

    #. Boot the VM. Sign in as ``student``, the password is ``ising-2021``
    #. Open a terminal, ``cd`` to the Ising_TDD project directory (``/home/student/Workspace/Ising_TDD/``)
    #. Issue ``make Ising_TDD_Arduino``

The build chain will now run through the various steps: it will build and run all unit tests, print some statistics, build the firmware, run a simulation-based integration test, show some more statistics and which ports fire what signals at what times, then flash the firmware to the Arduino and terminate. Should any of the tests fail, the build chain will terminate immediately, thus no erroneous code will be uploaded to the device.

The output will look somewhat like this: 

1. The unit tests are run.

.. image:: ../resources/img/unit_tests_output_I.jpg
  :width: 70%

... ... ...

.. image:: ../resources/img/unit_tests_output_II.jpg
  :width: 50%

2. The unit test coverage is reported.

.. image:: ../resources/img/unit_tests_coverage_report.jpg
  :width: 100%

3. The firmware is built and loaded into a simulation for an integration test, of which a coverage report is presented, as well as a port trace. The latter is opened in Gtkwave, the build will not proceed until the user closes the Gtkwave window.

.. image:: ../resources/img/integration_tests_coverage_report.jpg
  :width: 100%

.. image:: ../resources/img/gtkwave.jpg
  :width: 90%

Writing Your First Test
-----------------------

As a next step, we will write your first test. This project uses *Gtest*, a C++ unit testing framework from Google.
Let us assume that we want to add some new calculations to our Ising model simulation. A good directory to place them in would be ``src/algorithm/``. Remember that we want to work in a TDD mode, so we will devise the test **before** we work out the logic and regard it as a specification of what our code is supposed to do.

1. Create a source file ``src/algorithm/new_calculation__test.cpp``. Make sure to suffix all your tests with ``__test.cpp``. (While the ``__test`` part is not strictly necessary from a technical point of view, it is considered as good style to classify your tests as such in a way that is clear on a file system level, because it helps to maintain a good overview of our project.)

2. Add the following boilerplate code to the file:

.. code-block::
  :linenos:

  #include <gtest/gtest.h>

  TEST(IsingTddNewCalculationTests, OurFirstTest) {
    int our_test_variable = 1;
    const int our_expected_value = 1;

    ASSERT_EQ(our_test_variable, our_expected_value);
  }

3. Run ``make Ising_TDD_Arduino`` again. That's it! You have just successfully written and ran your first portion of a fully automated software test. Among the output, under the heading **"Running firmware unit tests"**, you should find the following:

.. image:: ../resources/img/our_first_unit_test.jpg
  :width: 55%

As an interlude, let us build some confidence in what automated software testing can do for us in the context of this project. Change the assertion of whether ``our_test_variable`` equals ``1`` to check against an expected value of ``2`` instead:

.. code-block::
  :linenos:

  #include <gtest/gtest.h>

  TEST(IsingTddNewCalculationTests, OurFirstTest) {
    int our_test_variable = 1;
    const int our_expected_value = 2;

    ASSERT_EQ(our_test_variable, our_expected_value);
  }

As a result, you will notice that the output of our test has changed:

.. image:: ../resources/img/our_first_unit_test_failure_I.jpg
  :width: 55%

What's more, the build chain comes to a halt:

.. image:: ../resources/img/our_first_unit_test_failure_II.jpg
  :width: 50%

This adds a lot of security and confidence to our code basis. Code that fails in any test is called out immediately with a precise error message on what has gone wrong and is never uploaded to the device.

Refining Your First Test
------------------------

To come to a better understanding of how software tests work with the Google test framework, let us dissect our example: 

.. code-block::
  :linenos:

  #include <gtest/gtest.h>

  TEST(IsingTddNewCalculationTests, OurFirstTest) {
    int our_test_variable = 1;
    const int our_expected_value = 1;

    ASSERT_EQ(our_test_variable, our_expected_value);
  }

- Line ``3`` tells the Google test framework that we are setting up a test via the ``TEST`` macro, which takes two arguments: the first is an identifier for our test suite, that is, a name under which we group a set of tests; the second is the name of the specific test we are implementing inside the braces ``{}``. Upon comparison of line ``3`` with the output screenshots, you will notice that Gtest prints ``<TestSuiteName>.<TestName>`` when running your tests.
- Line ``7`` performs an *assertion*. In this case, it simply compares both given arguments and throws an error if they do not equal. There are more assertions that Gtest can perform, but for the sake of simplicity, let us stick with just ``ASSERT_EQ``. Read `the Gtest documentation <https://google.github.io/googletest/primer.html>`_ when you're ready for more.

Now we should start developing our new calculation algorithm. Add the following lines to our example:

.. code-block::
  :linenos:
  :emphasize-lines: 1, 12-20, 22-29

  #include "new_calculation.hpp"
  
  #include <gtest/gtest.h>
  
  TEST(IsingTddNewCalculationTests, OurFirstTest) {
    int our_test_variable = 1; 
    const int our_expected_value = 1;
  
    ASSERT_EQ(our_test_variable, our_expected_value);
  }
  
  TEST(IsingTddNewCalculationTests, GettersAndSetters) {
    ising_tdd_algorithm::NewCalculation new_calculation;
  
    new_calculation.SetFirstValue(1);
    new_calculation.SetSecondValue(2);
  
    ASSERT_EQ(new_calculation.GetFirstValue(), 1);
    ASSERT_EQ(new_calculation.GetSecondValue(), 2);
  }
  
  TEST(IsingTddNewCalculationTests, CalculateSum) {
    ising_tdd_algorithm::NewCalculation new_calculation;
  
    new_calculation.SetFirstValue(1);
    new_calculation.SetSecondValue(2);
  
    ASSERT_EQ(new_calculation.CalculateSum(), 3);
  }

Run ``make Ising_TDD_Arduino`` once more. Of course, our build chain fails, because we have not implemented anything yet. 
Try to implement the ``ising_tdd_arduino::NewCalculation`` class and rerun the build chain. The build chain will only then complete successfully when you have implemented all requested features correctly.
The finished code should look something like this:

.. code-block::
  :linenos:

  #ifndef ISING_TDD_ALGORITHM_NEW_CALCULATION_HPP_
  #define ISING_TDD_ALGORITHM_NEW_CALCULATION_HPP_
  
  namespace ising_tdd_algorithm {
  
  class NewCalculation {
   public:
    explicit NewCalculation(): first_value_(0), second_value_(0) {}
    ~NewCalculation() {}
  
    int GetFirstValue() const { return first_value_; }
    int GetSecondValue() const { return second_value_; }
  
    void SetFirstValue(int value) { first_value_ = value; }
    void SetSecondValue(int value) { second_value_ = value; }
  
    int CalculateSum() { return first_value_ + second_value_; }
  
   private:
    int first_value_;
    int second_value_;
  };
  
  }
  
  #endif  // ISING_TDD_ALGORITHM_NEW_CALCULATION_HPP_

Before we finish our introduction to the Google test framework, we will have a look at one more feature: *fixtures*. Fixtures offer an elegant way to maintain a controlled testing environment and perform preparations and cleanups before and after tests.
Have another look at our example test suite: both of our most recently implemented tests require an instance of ``ising_tdd_algorithm::NewCalculation``. We will move the setting up of this variable to a common place, so that both tests can access it.
To do so, add another source file under ``src/algorithm/new_calculation__test_fixture.hpp`` and inside it, add a class whose name matches the name of our test suite (the identifier right after ``TEST(`` that all of our new tests share):

.. code-block::
  :linenos:

  #ifndef ISING_TDD_ALGORITHM_NEW_CALCULATION__TEST_FIXTURE_HPP_
  #define ISING_TDD_ALGORITHM_NEW_CALCULATION__TEST_FIXTURE_HPP_
  
  #include "new_calculation.hpp"
  
  #include <gtest/gtest.h>
  
  class IsingTddNewCalculationTests : public testing::Test {
   public:
    IsingTddNewCalculationTests() {}
  
    virtual ~IsingTddNewCalculationTests() {}
  
   protected:
    ising_tdd_algorithm::NewCalculation new_calculation_;
  };
  
  #endif  // ISING_TDD_ALGORITHM_NEW_CALCULATION__TEST_FIXTURE_HPP_

Next, modify the tests file as follows:

#. import the test fixture class,
#. replace all ocurrences of ``TEST(...)`` with ``TEST_F(...)`` and
#. remove the instantiation of the ``new_calculation`` variable and append a trailing ``_`` to the variable name on all other usages, so that it matches the name of the ``IsingTddNewCalculationTests::new_calculation_`` test fixture class variable.

The result will look like this:

.. code-block::
  :linenos:
  :emphasize-lines: 2, 6-8, 10-11, 14-16, 18

  #include "new_calculation.hpp"
  #include "new_calculation__test_fixture.hpp"
  
  #include <gtest/gtest.h>
  
  TEST_F(IsingTddNewCalculationTests, GettersAndSetters) {
    new_calculation_.SetFirstValue(1);
    new_calculation_.SetSecondValue(2);
  
    ASSERT_EQ(new_calculation_.GetFirstValue(), 1);
    ASSERT_EQ(new_calculation_.GetSecondValue(), 2);
  }
  
  TEST_F(IsingTddNewCalculationTests, CalculateSum) {
    new_calculation_.SetFirstValue(1);
    new_calculation_.SetSecondValue(2);
  
    ASSERT_EQ(new_calculation_.CalculateSum(), 3);
  }

TDD Cookbook
------------

Concluding, let us summarize the workflow we have just learned into a simple recipe. When you want to integrate a new software feature, do the following:

#. Add a new ``XXX__test.cpp`` file to the project. Also add a file named ``XXX__test_fixture.hpp``, if you want to set up a test fixture.
#. Work out a precise specification of the required software capabalities in the form of simple examples. Implement them as unit tests. Do not forget to mind fringe cases.
#. Implement the new software feature. Rerun the compiler frequently. Write just enough code to successfully pass all tests. The coverage report can give you a hint as to how close your implementation is to the specification defined through the tests.
#. TDD does not only concern new features. Use it also to maintain your code. When you encounter a bug, write a test that reproduces the bug **before** you fix it. When you adapt your code, adapt the tests first.

That's it! You know enough to get started with TDD and write safe, reliable, understandable code. Go out there and write some code!

.. figure:: ../resources/img/its_dangerous_to_go_alone.png
  :align: center

  `Source. <https://en.wikipedia.org/wiki/File:It%27s_dangerous_to_go_alone!_Take_this..png>`_ *© by Nintendo. Fair use.*

