Introduction
============

   *"[Clean code that works] gives you a chance to learn all of the lessons that the code has to teach you."*

      *~ Kent Beck, Test Driven Development By Example*

Test Driven Development
-----------------------
TDD is an approach to software development where requirements for software components are defined through a set of automated tests that are implemented **prior to** the actual code providing the desired software features. 

Motivation
----------
The Ising_TDD project was conceived by Jonathan Debus in an attempt to integrate an existing Ising model simulation into an ecosystem that allows for simple test driven development (TDD).

The starting point was a Monte Carlo simulation of a two dimensional spin lattice of dimensions 10x14, implemented in bare metal C, run on a custom PCB equipped with a Microchip Technology (former Atmel) ATmega645 and a 10x14 LED matrix. The project was (and currently is) used as a basis for student projects of various kinds at the *Bergische Universität Wuppertal*.

From the author's experience, students often have difficulties when faced with the task of programming on embedded devices, presumably stemming from the reduced accessibility of code execution feedback, such as error messages, exception traces or runtime inspection functionalities supplied by debuggers. 

The prime focus of software design choices thus was to

  - secure the project's core components against the introduction of errors and to
  - facilitate the writing of new tests to a degree where students essentially only need to string together assertions inside some boilerplate code that they do not even necessarily fully understand.

Installation
------------
The project is distributed as a VM running *Endeavour OS*, an *Arch Linux* based distribution. All required software packages are included in compatible versions, so there is no need for any additional installations.

The download can be found `here <https://www.mediafire.com/file/caa4uti9cx0v8y1/ENDEAVOUR_ISING_TDD_DIST.ova/file>`_.

Setup
-----
The project can either be setup to run on the aforementioned custom PCB or on an `Arduino Uno <https://www.reichelt.de/arduino-uno-rev-3-dip-variante-atmega328-usb-arduino-uno-dip-p154902.html>`_ fitted with an ST7735 based `TFT screen <https://www.reichelt.de/entwicklerboards-display-led-1-8-128x160-st7735r-debo-tft-1-8-p192139.html>`_.

