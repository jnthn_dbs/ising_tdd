Hardware Abstraction Layer
--------------------------

.. toctree::

   hal/hal
   hal/arduino_uno_st7735
   hal/atmega645_pcb
   hal/tests

