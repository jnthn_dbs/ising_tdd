Examples
........

.. code-block::

  /**
   * Two-times-two lattice:
   *
   * +---+---+
   * | X | X |
   * +---+---+
   * | X | X |
   * +---+---+
   */
  ising_tdd_state::Lattice<2, 2> lattice;

  /**
   * First row of that lattice:
   *
   * +---+---+
   * | X | X |
   * +---+---+
   * |   |   |
   * +---+---+
   */
  ising_tdd_state::SubLattice<2> subLattice = lattice[0];
  
  /**
   * First spin in this row:
   *
   * +---+---+
   * | X |   |
   * +---+---+
   * |   |   |
   * +---+---+
   */
  ising_tdd_state::Spin spin = subLattice[0];

