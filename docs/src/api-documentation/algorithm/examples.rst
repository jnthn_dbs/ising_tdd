Examples
........

.. code-block::

    ising_tdd_algorithm::Algorithm algorithm(10., 0.);

    ising_tdd_state::Lattice<3> oneDimLattice;
    ising_tdd_state::Lattice<3, 3> twoDimLattice;

    oneDimLattice.InitHot();
    twoDimLattice.InitHot();

    algorithm.CalculateSpin(oneDimLattice[1],
                            oneDimLattice[0], oneDimLattice[2]);

    algorithm.CalculateSpin(twoDimLattice[1][1],
                            twoDimLattice[0][1], twoDimLattice[1][2],
                            twoDimLattice[2][1], twoDimLattice[1][0]);

    // lower temperature, add magnetic field
    algorithm.SetInverseTemperature(100.0);
    algorithm.SetMagneticFieldStrength(1.0);

    algorithm.CalculateSpin(oneDimLattice[1],
                            oneDimLattice[0], oneDimLattice[2]);

    algorithm.CalculateSpin(twoDimLattice[1][1],
                            twoDimLattice[0][1], twoDimLattice[1][2],
                            twoDimLattice[2][1], twoDimLattice[1][0]);

