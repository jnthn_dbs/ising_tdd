.SILENT:
.PHONY:

ISING_TDD_UNIT_TESTS_INPUT_FILES = src/main.cpp src/algorithm/*.cpp src/state/*.cpp
ISING_TDD_UNIT_TESTS_INCLUDE_DIRS = -Isrc/algorithm -Isrc/state

ISING_TDD_INTEGRATION_TESTS_SHOW_PORT_TRACE = 1

ISING_TDD_ARDUINO_APPLICATION_FILES = src/algorithm/algorithm.hpp \
                                      src/hal/abstract_device.hpp \
                                      src/hal/arduino_st7735.hpp \
																			src/state/lattice.hpp \
																			src/state/spin.hpp

ifeq ($(findstring Ising_TDD_Arduino, $(MAKECMDGOALS)), Ising_TDD_Arduino)
ISING_TDD_TARGET_PLATFORM := Ising_TDD_Arduino
ISING_TDD_BOARD_FLAG := -DISING_TDD_BOARD=1
ISING_TDD_TRACE_FILE := arduino_st7735__sim.vcd
else ifeq ($(findstring Ising_TDD_PpHoelbling, $(MAKECMDGOALS)), Ising_TDD_PpHoelbling)
ISING_TDD_TARGET_PLATFORM := Ising_TDD_PpHoelbling
ISING_TDD_BOARD_FLAG = -DISING_TDD_BOARD=2
ISING_TDD_TRACE_FILE := pp_hoelbling__sim.vcd
endif

default:
	   echo -e " " \
	&& echo -e "  \e[106;95m#####################################################################################\e[0m" \
	&& echo -e "  \e[106;95m#  Please choose a make target that fits your hardware platform.                    #\e[0m" \
	&& echo -e "  \e[106;95m#                                                                                   #\e[0m" \
	&& echo -e "  \e[106;95m#  Available platforms:                                                             #\e[0m" \
	&& echo -e "  \e[106;95m#    - Ising_TDD_PpHoelbling (Custom ATmega645 based PCB)                           #\e[0m" \
	&& echo -e "  \e[106;95m#    - Ising_TDD_Arduino (Aruino Uno with ATmega328P and ST7735 based TFT display)  #\e[0m" \
	&& echo -e "  \e[106;95m#                                                                                   #\e[0m" \
	&& echo -e "  \e[106;95m#  Available tests:                                                                 #\e[0m" \
	&& echo -e "  \e[106;95m#    - Ising_TDD_Complete_Unit_Tests                                                #\e[0m" \
	&& echo -e "  \e[106;95m#    - Ising_TDD_Arduino_Complete_Simulation                                        #\e[0m" \
	&& echo -e "  \e[106;95m#    - Ising_TDD_PpHoelbling_Complete_Simulation                                    #\e[0m" \
	&& echo -e "  \e[106;95m#                                                                                   #\e[0m" \
	&& echo -e "  \e[106;95m#  Other available targets:                                                         #\e[0m" \
	&& echo -e "  \e[106;95m#    - documentation (Build the docs)                                               #\e[0m" \
	&& echo -e "  \e[106;95m#    - clean (Delete all build output products)                                     #\e[0m" \
	&& echo -e "  \e[106;95m#####################################################################################\e[0m" \
	&& echo -e " "

clean:
	   echo -e " " \
	&& echo -e "  \e[106;95m###########################\e[0m" \
	&& echo -e "  \e[106;95m#  Removing output files  #\e[0m" \
	&& echo -e "  \e[106;95m###########################\e[0m" \
	&& echo -e " " \
	&& sleep 1 \
	&& rm -vf arduino/*.ino arduino/*.hpp arduino/*.cpp arduino/build/* build/* output/*

documentation:
	   echo -e " " \
	&& echo -e "  \e[106;95m############################\e[0m" \
	&& echo -e "  \e[106;95m#  Building documentation  #\e[0m" \
	&& echo -e "  \e[106;95m############################\e[0m" \
	&& echo -e " " \
	&& sleep 1 \
	&& cd docs/ \
	&& doxygen \
	&& make clean \
	&& make html \
	&& echo -e " " \
	&& echo -e "  \e[106;95m#################################################################################\e[0m" \
	&& echo -e "  \e[106;95m#  To read the documentation, open '\e[1;5;106;95mdocs/_build/html/index.html\e[0;106;95m' in a browser!  #\e[0m" \
	&& echo -e "  \e[106;95m#################################################################################\e[0m" \
	&& echo -e " "

Ising_TDD_Arduino: Ising_TDD_Arduino_Complete_Simulation
	   echo -e " " \
	&& echo -e "  \e[106;95m########################\e[0m" \
	&& echo -e "  \e[106;95m#  Uploading firmware  #\e[0m" \
	&& echo -e "  \e[106;95m########################\e[0m" \
	&& echo -e " " \
	&& sleep 1 \
	&& cd arduino \
	&& arduino-cli compile --fqbn arduino:avr:uno --output-dir build/ \
	   --build-property build.extra_flags="-std=c++17 -DISING_TDD_BOARD=1" \
     --upload --port /dev/ttyS2 \
	&& echo -e " " \
	&& echo -e "  \e[106;95m###########################\e[0m" \
	&& echo -e "  \e[106;95m#  Finished successfully  #\e[0m" \
	&& echo -e "  \e[106;95m###########################\e[0m" \
	&& echo -e " "

# use atmega128 for simulation!
Ising_TDD_PpHoelbling: Ising_TDD_PpHoelbling_Complete_Simulation
	   echo -e "  \e[106;95m#############################\e[0m" \
	&& echo -e "  \e[106;95m#  Building final firmware  #\e[0m" \
	&& echo -e "  \e[106;95m#############################\e[0m" \
	&& avr-g++ -std=c++20 \
	   src/main.cpp \
		 -Isrc/algorithm -Isrc/hal -Isrc/state \
		 -o build/Ising_TDD_PpHoelbling \
		 -mmcu=atmega645 \
		 -DISING_TDD_BOARD=2 -DISING_TDD_COMPILER_SUPPORTS_CPP20 \
	&& avrdude -vvv -b 115200 -p atmega645 -P /dev/ttyS2 -U flash:w:build/Ising_TDD_PpHoelbling:a \
	&& echo -e " " \
	&& echo -e "  \e[106;95m###########################\e[0m" \
	&& echo -e "  \e[106;95m#  Finished successfully  #\e[0m" \
	&& echo -e "  \e[106;95m###########################\e[0m" \
	&& echo -e " "

Ising_TDD_Arduino_Complete_Simulation: Ising_TDD_Complete_Simulation

Ising_TDD_PpHoelbling_Complete_Simulation: Ising_TDD_Complete_Simulation

Ising_TDD_Complete_Simulation: Ising_TDD_Build_Simulation
	   echo -e " " \
	&& echo -e "  \e[106;95m########################\e[0m" \
	&& echo -e "  \e[106;95m#  Running simulation  #\e[0m" \
	&& echo -e "  \e[106;95m########################\e[0m" \
	&& echo -e " " \
	&& sleep 1 \
	&& LLVM_PROFILE_FILE="output/coverage_simulation.profraw" ./build/$(ISING_TDD_TARGET_PLATFORM)_Simulation \
	&& llvm-profdata merge -sparse output/coverage_simulation.profraw -o output/coverage_simulation.profdata \
	&& echo -e " " \
	&& echo -e "  \e[106;95m###################################\e[0m" \
	&& echo -e "  \e[106;95m#  Reporting simulation coverage  #\e[0m" \
	&& echo -e "  \e[106;95m###################################\e[0m" \
	&& echo -e " " \
	&& sleep 1 \
	&& llvm-cov report ./build/$(ISING_TDD_TARGET_PLATFORM)_Simulation -instr-profile=output/coverage_simulation.profdata \
	&& sleep 3 \
	&& if [ $(ISING_TDD_INTEGRATION_TESTS_SHOW_PORT_TRACE) -ne 0 ]; \
	   then \
          echo -e " " \
       && echo -e "  \e[106;95m##################################\e[0m" \
       && echo -e "  \e[106;95m#  Presenting port trace         #\e[0m" \
       && echo -e "  \e[1;5;106;95m#  Close Gtkwave to resume build #\e[0m" \
       && echo -e "  \e[106;95m##################################\e[0m" \
       && echo -e " " \
       && sleep 4 \
       && gtkwave output/$(ISING_TDD_TRACE_FILE); \
     fi

Ising_TDD_Build_Simulation: $(ISING_TDD_TARGET_PLATFORM)_Build_Firmware
	   echo -e " " \
	&& echo -e "  \e[106;95m#########################\e[0m" \
	&& echo -e "  \e[106;95m#  Building simulation  #\e[0m" \
	&& echo -e "  \e[106;95m#########################\e[0m" \
	&& echo -e " " \
	&& sleep 1 \
	&& clang++ \
	   -g -O0 -fprofile-instr-generate -fcoverage-mapping \
	   -std=c++20 \
	   src/main.cpp \
		 -Isrc/hal -lsimavr -lelf -o build/$(ISING_TDD_TARGET_PLATFORM)_Simulation \
		 $(ISING_TDD_BOARD_FLAG) -DISING_TDD_SIMULATION -DISING_TDD_COMPILER_SUPPORTS_CPP20

Ising_TDD_Arduino_Build_Firmware: Ising_TDD_Complete_Unit_Tests
	   echo -e " " \
	&& echo -e "  \e[106;95m#######################\e[0m" \
	&& echo -e "  \e[106;95m#  Building firmware  #\e[0m" \
	&& echo -e "  \e[106;95m#######################\e[0m" \
	&& echo -e " " \
	&& sleep 1 \
	&& cp src/main.cpp arduino/arduino.ino \
	&& for ising_tdd_arduino_application_file in $(ISING_TDD_ARDUINO_APPLICATION_FILES);\
	   do\
		   cp $$ising_tdd_arduino_application_file arduino/;\
		 done \
	&& cd arduino \
	&& arduino-cli compile --fqbn arduino:avr:uno --output-dir build/ \
	   --build-property build.extra_flags="-std=c++17 -DISING_TDD_BOARD=1" \
	&& cd ..

Ising_TDD_PpHoelbling_Build_Firmware: Ising_TDD_Complete_Unit_Tests
	   echo -e "  \e[106;95m######################################\e[0m" \
	&& echo -e "  \e[106;95m#  Building firmware for simulation  #\e[0m" \
	&& echo -e "  \e[106;95m######################################\e[0m" \
	&& avr-g++ -std=c++20 \
	   src/main.cpp \
		 -Isrc/algorithm -Isrc/hal -Isrc/state \
		 -o build/Ising_TDD_PpHoelbling \
		 -mmcu=atmega128 \
		 -DISING_TDD_BOARD=2 -DISING_TDD_PPHOELBLING_SIMULATION -DISING_TDD_COMPILER_SUPPORTS_CPP20

Ising_TDD_Complete_Unit_Tests: Ising_TDD_Build_Unit_Tests
	   echo -e " " \
	&& echo -e "  \e[106;95m########################\e[0m" \
	&& echo -e "  \e[106;95m#  Running unit tests  #\e[0m" \
	&& echo -e "  \e[106;95m########################\e[0m" \
	&& echo -e " " \
	&& sleep 1 \
	&& LLVM_PROFILE_FILE="output/coverage_unit_tests.profraw" ./build/Ising_TDD_Unit_Tests \
	&& sleep 1 \
	&& llvm-profdata merge -sparse output/coverage_unit_tests.profraw -o output/coverage_unit_tests.profdata \
	&& echo -e " " \
	&& echo -e "  \e[106;95m##################################\e[0m" \
	&& echo -e "  \e[106;95m#  Reporting unit test coverage  #\e[0m" \
	&& echo -e "  \e[106;95m##################################\e[0m" \
	&& echo -e " " \
	&& sleep 1 \
	&& llvm-cov report ./build/Ising_TDD_Unit_Tests -instr-profile=output/coverage_unit_tests.profdata \
	&& sleep 3

Ising_TDD_Build_Unit_Tests: Ising_TDD_Splashscreen
	   echo -e " " \
	&& echo -e "  \e[106;95m#########################\e[0m" \
	&& echo -e "  \e[106;95m#  Building unit tests  #\e[0m" \
	&& echo -e "  \e[106;95m#########################\e[0m" \
	&& echo -e " " \
	&& sleep 1 \
	&& clang++ \
	   -g -O0 -fprofile-instr-generate -fcoverage-mapping \
	   -std=c++20 \
		 $(ISING_TDD_UNIT_TESTS_INPUT_FILES) \
		 $(ISING_TDD_UNIT_TESTS_INCLUDE_DIRS) \
		 -lgtest \
		 -o build/Ising_TDD_Unit_Tests \
		 -DISING_TDD_BOARD=3 -DISING_TDD_TESTING -DISING_TDD_COMPILER_SUPPORTS_CPP20 \

Ising_TDD_Splashscreen:
	   echo -e " " \
	&& echo -e " " \
	&& echo -e "  \e[40;93m┎────────────────────────────────────────────────────────────────────────────────────────────────────────────┐  \e[0m" \
	&& echo -e "  \e[40;93m┃ \e[106;30m                                                                                                          \e[40;93m │\e[0m" \
	&& echo -e "  \e[40;93m┃ \e[106;30m                                                                                                          \e[40;93m │\e[0m" \
	&& echo -e "  \e[40;93m┃ \e[106;30m            ┎─┐             ┎──────┐             ┎─┐            ┎─┐  ┎─┐            ┎───────┐             \e[40;93m │\e[0m" \
	&& echo -e "  \e[40;93m┃ \e[106;30m            ┃ │             ┃ ┍━━━━┙             ┃ │            ┃ └┐ ┃ │            ┃ ┍━━━━━┙             \e[40;93m │\e[0m" \
	&& echo -e "  \e[40;93m┃ \e[106;30m            ┃ │             ┃ └─────┐            ┃ │            ┃  └┐┃ │            ┃ │  ┎───┐            \e[40;93m │\e[0m" \
	&& echo -e "  \e[40;93m┃ \e[106;30m            ┃ │             ┗━━━━━┓ │            ┃ │            ┃ ┍┓└┚ │            ┃ │  ┗━┓ │            \e[40;93m │\e[0m" \
	&& echo -e "  \e[40;93m┃ \e[106;30m            ┃ │                   ┃ │            ┃ │            ┃ │┗┓  │            ┃ │    ┃ │            \e[40;93m │\e[0m" \
	&& echo -e "  \e[40;93m┃ \e[106;30m            ┃ │            ┎──────┚ │            ┃ │            ┃ │ ┗┓ │            ┃ └────┚ │            \e[40;93m │\e[0m" \
	&& echo -e "  \e[40;93m┃ \e[106;30m            ┗━┙            ┗━━━━━━━━┙            ┗━┙            ┗━┙  ┗━┙            ┗━━━━━━━━┙            \e[40;93m │\e[0m" \
	&& echo -e "  \e[40;93m┃ \e[106;30m            ┗━┙            ┗━━━━━━━━┙            ┗━┙            ┗━┙  ┗━┙            ┗━━━━━━━━┙            \e[40;93m │\e[0m" \
	&& echo -e "  \e[40;93m┃ \e[106;30m            ┗━┙            ┗━━━━━━━━┙            ┗━┙            ┗━┙  ┗━┙            ┗━━━━━━━━┙            \e[40;93m │\e[0m" \
	&& echo -e "  \e[40;93m┃ \e[106;30m                                                                                                          \e[40;93m │\e[0m" \
	&& echo -e "  \e[40;93m┃ \e[106;90m                                                                                                          \e[40;93m │\e[0m" \
	&& echo -e "  \e[40;93m┃ \e[106;95m    ██████████████████████████████    ████████████████████              ████████████████████              \e[40;93m │\e[0m" \
	&& echo -e "  \e[40;93m┃ \e[106;95m   ▒██████████████████████████████   ▒████████████████████             ▒████████████████████              \e[40;93m │\e[0m" \
	&& echo -e "  \e[40;93m┃ \e[106;95m  ░▒██████████████████████████████  ░▒████████████████████            ░▒████████████████████              \e[40;93m │\e[0m" \
	&& echo -e "  \e[40;93m┃ \e[106;95m  ░▒██████████████████████████████  ░▒████████████████████            ░▒████████████████████              \e[40;93m │\e[0m" \
	&& echo -e "  \e[40;93m┃ \e[106;95m  ░▒▒▒▒▒▒▒▒▒▒▒██████████▒▒▒▒▒▒▒▒▒   ░▒██████████▒▒▒▒▒██████████       ░▒██████████▒▒▒▒▒██████████         \e[40;93m │\e[0m" \
	&& echo -e "  \e[40;93m┃ \e[106;95m  ░░░░░░░░░░░▒██████████░░░░░░░░    ░▒██████████░░░░▒██████████       ░▒██████████░░░░▒██████████         \e[40;93m │\e[0m" \
	&& echo -e "  \e[40;93m┃ \e[106;95m            ░▒██████████            ░▒██████████   ░▒▒▒▒▒▒██████████  ░▒██████████   ░▒▒▒▒▒▒██████████    \e[40;93m │\e[0m" \
	&& echo -e "  \e[40;93m┃ \e[106;95m            ░▒██████████            ░▒██████████   ░░░░░░▒██████████  ░▒██████████   ░░░░░░▒██████████    \e[40;93m │\e[0m" \
	&& echo -e "  \e[40;93m┃ \e[106;95m            ░▒██████████            ░▒██████████        ░▒██████████  ░▒██████████        ░▒██████████    \e[40;93m │\e[0m" \
	&& echo -e "  \e[40;93m┃ \e[106;95m            ░▒██████████            ░▒██████████        ░▒██████████  ░▒██████████        ░▒██████████    \e[40;93m │\e[0m" \
	&& echo -e "  \e[40;93m┃ \e[106;95m            ░▒██████████            ░▒██████████        ░▒██████████  ░▒██████████        ░▒██████████    \e[40;93m │\e[0m" \
	&& echo -e "  \e[40;93m┃ \e[106;95m            ░▒██████████            ░▒██████████        ░▒██████████  ░▒██████████        ░▒██████████    \e[40;93m │\e[0m" \
	&& echo -e "  \e[40;93m┃ \e[106;95m            ░▒██████████            ░▒██████████████████████████████  ░▒██████████████████████████████    \e[40;93m │\e[0m" \
	&& echo -e "  \e[40;93m┃ \e[106;95m            ░▒██████████            ░▒██████████████████████████████  ░▒██████████████████████████████    \e[40;93m │\e[0m" \
	&& echo -e "  \e[40;93m┃ \e[106;95m            ░▒██████████            ░▒██████████████████████████████  ░▒██████████████████████████████    \e[40;93m │\e[0m" \
	&& echo -e "  \e[40;93m┃ \e[106;95m            ░▒██████████            ░▒██████████████████████████████  ░▒██████████████████████████████    \e[40;93m │\e[0m" \
	&& echo -e "  \e[40;93m┃ \e[106;95m            ░▒▒▒▒▒▒▒▒▒▒             ░▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒   ░▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒     \e[40;93m │\e[0m" \
	&& echo -e "  \e[40;93m┃ \e[106;95m            ░░░░░░░░░░              ░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░    ░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░      \e[40;93m │\e[0m" \
	&& echo -e "  \e[40;93m┃ \e[106;95m                                                                                                          \e[40;93m │\e[0m" \
	&& echo -e "  \e[40;93m┗━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━jdphysik@gmail.com━━━━━━━━━┙  \e[0m" \
	&& echo -e " " \
	&& echo -e " " \
	&& sleep 2

