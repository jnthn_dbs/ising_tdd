#ifndef ISING_TDD_ALGORITHM_ALGORITHM_HPP_
#define ISING_TDD_ALGORITHM_ALGORITHM_HPP_

#include "lattice.hpp"
#include "spin.hpp"

#if ISING_TDD_BOARD != ISING_TDD_BOARD_ARDUINO
#include <math.h>
#include <stdint.h>
#include <stdlib.h>
#endif  // ISING_TDD_BOARD != ISING_TDD_BOARD_ARDUINO

namespace ising_tdd_algorithm {

/**
 * @brief Calculate probabilities for spin flips.
 *
 * Owns information about the environment of a lattice. See
 * @ref ising_tdd_algorithm::Algorithm::inverse_temperature_ and
 * @ref ising_tdd_algorithm::Algorithm::magnetic_field_strength_.
 *
 * Must be supplied with a definition of a lattice point to calculate a
 * probability. See @ref ising_tdd_algorithm::Algorithm::CalculateSpin.
 */
class Algorithm final {
 public:
  explicit Algorithm(float inverse_thermal_energy,
                     float magnetic_field_strength):
      inverse_temperature_(inverse_thermal_energy),
      magnetic_field_strength_(magnetic_field_strength) {}
  
  ~Algorithm() {}
 
  /**
   * @brief Update the orientation of a given spin.
   *
   * The probability @f$P@f$ to flip a spin is given by
   *
   * @f{eqnarray*}{
   *   P  &=& \exp(dE), \\
   *   \text{where} &\ & \\
   *   dE &=& 2T (2nn - N) \pm 2h, \\
   *   \text{where} &\ & \\
   *   T  &=& \text{Inverse temperature}, \\
   *   nn &=& \text{Number of nearest neighbors with opposite spin}, \\
   *   N  &=& \text{Total number of nearest neighbors}, \\
   *   h  &=& \text{Magnetic field strength}.
   * @f}
   *
   * The following table shows some example values. In absence of a magnetic
   * field, a spin will always flip if at least half its neighbors are oriented
   * opposingly.
   *
   * @code{.unparsed}
   * +------------------+-----------------------+
   * |     exp(dE)      |        nn / N         |
   * |                  +-----------------------+
   * |      h = 0       | 0 / 2 | 0 / 4 | 1 / 4 |
   * +---------+--------+-------+-------+-------+
   * |         |    1.0 | .0183 | .0003 | .0183 |
   * |         |   10.0 | .6703 | .4493 | .6703 |
   * |    T    |  100.0 | .9608 | .9231 | .9608 |
   * |         |  300.0 | .9868 | .9737 | .9868 |
   * |         | 1000.0 | .9960 | .9920 | .9960 |
   * +---------+--------+-------+-------+-------+
   * @endcode
   *
   */
  template<typename ... Spins>
  void CalculateSpin(ising_tdd_state::Spin spin, Spins ... neighbors) {
    float energy_difference =
        2. *
          inverse_temperature_ *
          (2. * CalculateNextNeighbors(spin, neighbors ...) -
           sizeof...(neighbors)) +
        (spin == ising_tdd_state::SpinValue::kSpinUp ? -2. : 2.) *
          magnetic_field_strength_;

#if ISING_TDD_BOARD != ISING_TDD_BOARD_ARDUINO
    if ((float)exp(energy_difference) >= (float)rand() / (float)RAND_MAX) {
#else
    if ((float)pow(2.7182818284590452353602875, energy_difference) >=
        (float)random(2147483647) / (2147483647 - 1)) {
#endif  // ISING_TDD_BOARD != ISING_TDD_BOARD_ARDUINO
      spin.Flip();
    }
  }

  void SetInverseTemperature(float inverse_temperature) {
    inverse_temperature_ = inverse_temperature;
  }
  
  void SetMagneticFieldStrength(float magnetic_field_strength) {
    magnetic_field_strength_ = magnetic_field_strength;
  }

 private:
  template<typename ... Spins>
  uint8_t CalculateNextNeighbors(ising_tdd_state::Spin spin,
                                 ising_tdd_state::Spin neighbor,
                                 Spins ... neighbors) {
    if constexpr (sizeof...(neighbors) == 0) {
      return spin != neighbor;
    } else {
      return (spin != neighbor) + CalculateNextNeighbors(spin, neighbors ...);
    }
  }

  float inverse_temperature_;  // beta
  float magnetic_field_strength_;  // h
};

}

#endif  // ISING_TDD_ALGORITHM_ALGORITHM_HPP_

