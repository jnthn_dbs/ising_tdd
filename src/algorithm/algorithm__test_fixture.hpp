#ifndef ISING_TDD_ALGORITHM__TEST_FIXTURE_HPP_
#define ISING_TDD_ALGORITHM__TEST_FIXTURE_HPP_

#define ISING_TDD_ALGORITHM__TEST_N_RANDOM_NUMBERS 10000

#include "algorithm.hpp"

#include <stdlib.h>

#include <gtest/gtest.h>

/**
 * @brief Test fixture for tests of @ref ising_tdd_algorithm::Algorithm. Owns
 *        an instance of the tested class and a buffer with random floats.
 */
class IsingTddAlgorithmTests : public testing::Test {
 public:
  IsingTddAlgorithmTests(): algorithm_(.1, 0.) {}

  virtual ~IsingTddAlgorithmTests() {}
 
 protected:
  void PrepareRandomNumbers() {
    srand(17082019);

    for (unsigned int i = 0;
         i < ISING_TDD_ALGORITHM__TEST_N_RANDOM_NUMBERS;
         ++i) {
      random_numbers_[i] = (float)rand() / (float)RAND_MAX;
    }

    srand(17082019);
  }

  ising_tdd_algorithm::Algorithm algorithm_;
  float random_numbers_[ISING_TDD_ALGORITHM__TEST_N_RANDOM_NUMBERS];
};

#endif  // ISING_TDD_ALGORITHM__TEST_FIXTURE_HPP_

