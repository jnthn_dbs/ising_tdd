#include "algorithm.hpp"
#include "algorithm__test_fixture.hpp"

#include "lattice.hpp"
#include "spin.hpp"

#include <math.h>

#include <gtest/gtest.h>

/**
 * @brief @anchor test_one_dimensional_cold_lattice Test whether
 *        @ref ising_tdd_algorithm::Algorithm::CalculateSpin manipulates spins
 *        correctly according to a known sequence of pseudo-random numbers.
 */
TEST_F(IsingTddAlgorithmTests, OneDimensionalColdLattice) {
  ising_tdd_state::Lattice<3> lattice;

  lattice.InitCold();

  PrepareRandomNumbers();

  const float kFlipProbability = exp(2.0 * .1 * (2.0 * 0 - 2));  // ~.6703

  unsigned int n_flips = 0;
 
  for (unsigned int i = 0;
       i < ISING_TDD_ALGORITHM__TEST_N_RANDOM_NUMBERS;
       ++i) {
    algorithm_.CalculateSpin(lattice[1], lattice[0], lattice[2]);

    if (kFlipProbability >= random_numbers_[i]) {
      ASSERT_EQ(lattice[1] == ising_tdd_state::SpinValue::kSpinUp, true);

      ++n_flips;

      lattice[1] = ising_tdd_state::SpinValue::kSpinDown;
    } else {
      ASSERT_EQ(lattice[1] == ising_tdd_state::SpinValue::kSpinDown, true);
    }
  }

  const double kMeasuredFlipProbability = (double)n_flips /
      ISING_TDD_ALGORITHM__TEST_N_RANDOM_NUMBERS;

  EXPECT_GT(kMeasuredFlipProbability, kFlipProbability - .05);
  EXPECT_LT(kMeasuredFlipProbability, kFlipProbability + .05);
}

/**
 * @brief Test whether @ref ising_tdd_algorithm::Algorithm::CalculateSpin always
 *        flips a spin when the probability is greater or equal to 1.
 */
TEST_F(IsingTddAlgorithmTests, OneDimensionalInstableLattice) {
  ising_tdd_state::Lattice<3> lattice;

  lattice[0] = ising_tdd_state::SpinValue::kSpinUp;
  lattice[1] = ising_tdd_state::SpinValue::kSpinDown;
  lattice[2] = ising_tdd_state::SpinValue::kSpinUp;

  for (int i = 0; i < 1000; ++i) {
    algorithm_.CalculateSpin(lattice[1], lattice[0], lattice[2]);

    ASSERT_EQ(lattice[1] == ising_tdd_state::SpinValue::kSpinUp, true);

    lattice[1] = ising_tdd_state::SpinValue::kSpinDown;
  }
}

/**
 * @brief Repeat @ref test_one_dimensional_cold_lattice
 *        "TEST(IsingTddAlgorithmTests, OneDimensionalColdLattice)" with two
 *        dimensions.
 */
TEST_F(IsingTddAlgorithmTests, TwoDimensionalColdLattice) {
  ising_tdd_state::Lattice<3, 3> lattice;

  lattice.InitCold();

  PrepareRandomNumbers();

  const float kFlipProbability = exp(2.0 * .1 * (2.0 * 0 - 4));  // ~.4493

  unsigned int n_flips = 0;
 
  for (unsigned int i = 0;
       i < ISING_TDD_ALGORITHM__TEST_N_RANDOM_NUMBERS;
       ++i) {
    algorithm_.CalculateSpin(lattice[1][1], lattice[1][0], lattice[1][2],
                                            lattice[0][1], lattice[2][1]);

    if (kFlipProbability >= random_numbers_[i]) {
      ASSERT_EQ(lattice[1][1] == ising_tdd_state::SpinValue::kSpinUp, true);

      ++n_flips;

      lattice[1][1] = ising_tdd_state::SpinValue::kSpinDown;
    } else {
      ASSERT_EQ(lattice[1][1] == ising_tdd_state::SpinValue::kSpinDown, true);
    }
  }

  const double kMeasuredFlipProbability = (double)n_flips /
      ISING_TDD_ALGORITHM__TEST_N_RANDOM_NUMBERS;

  EXPECT_GT(kMeasuredFlipProbability, kFlipProbability - .05);
  EXPECT_LT(kMeasuredFlipProbability, kFlipProbability + .05);
}

