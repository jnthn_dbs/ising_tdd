#ifndef ISING_TDD_HAL_PP_HOELBLING_HPP_
#define ISING_TDD_HAL_PP_HOELBLING_HPP_

#include "abstract_device.hpp"
// #include "lattice.hpp"
// #include "spin.hpp"

#define F_CPU 8000000L
#include <stdlib.h>
#include <avr/io.h>
#include <avr/eeprom.h>
#include <avr/interrupt.h>
#include <util/delay.h>
#include <avr/sleep.h>
#include <math.h>

/* lattice size */
#define NX 10
#define NY 14

/* bit offset, i.e. first bit used for a spin */
#define YOFS 1

/* temperature scaling constants */
#define BETAC 0.4406867935097715126
#define TMID 330.0
#define TPOW 2

/* Hal sensor zero and gain */
#define HALGAIN 0.01

#define INIT_COLD 0
#define INIT_HOT 1
#define INIT_ANTIFER 2
#define INIT_FULL 3

float M0 = -1.0;

/* seed saved to eeprom */
long ee_seed EEMEM;

/* the lattice, spins coded in bits */
uint16_t spin[NX];

// the inverse temperature and magnetic field
//float beta=logf(1+sqrtf(2))/2;
float beta = 0.4406867935097715126;
float h = 0.0;

// ferromagnetic(1)/antiferromagnetic(-1) coupling
int8_t faf = 1;

// periodic(0)/antiperiodic(1) BC
uint8_t bc = 0;

uint8_t okeyA5; // initial key state
uint8_t okeyB7; // initial key state
uint8_t keyA5 = 0; // @todo unused?
uint8_t keyB7 = 0; // @todo unused?

// state of the toggle key
uint8_t tk = 1;

// state of the toggle key
uint8_t ak = 1;

/* current display column */
uint8_t cc=0;

// on/off
uint8_t einaus;
uint8_t ee_einaus EEMEM;

// display one column of pixar ISR
ISR (TIMER1_COMPA_vect)
{
  if (cc<8) // current column off
    DDRD&=~(1<<cc);
  else
    DDRG&=~(1<<(cc-8));
  
  cc=(cc+1)%NY; // increment column
  PORTA=(PORTA&0b10000000)|((~spin[cc]>>YOFS)&0b01111111); // display new column, low bits
  PORTC=(PORTC&0b10000000)|((~spin[cc]>>(YOFS+7))&0b01111111); // display new column, high bits
  
  if (cc<8) // current column on
    DDRD|=(1<<cc);
  else
    DDRG|=(1<<(cc-8));
}

/* define empty ADC interrupt */
EMPTY_INTERRUPT(ADC_vect);

uint8_t rnd(uint8_t max)
{
  return random() / ((RANDOM_MAX/max)+1);
}

// display a number in binary format
void numdisp(uint32_t num, uint8_t ofs)
{
  uint8_t i;

  for (i = 0; i < 3; ++i) {
    spin[i+ofs] = (num & 0b11111111111111) << YOFS;
    num >>= NY;
  }
}

uint16_t get_adc_hal()
{
  uint16_t adcr;

  // select channel 1 and AVcc reference with external capacitor
  ADMUX = 0b01000001;
  
  // warm up the ADC, discard the first conversion
  ADCSRA |= 1 << ADSC;
  
  while (ADCSRA & (1 << ADSC)) {
    ;
  }

  ADCSRA |= 1 << ADSC; // start single conversion
  
  // wait until conversion is done
  while (ADCSRA & (1 << ADSC)) {
    ;
  }

  adcr = ADC; // read out result

  // finally set up temperature ADC
  ADMUX = 0b11000000;

  return adcr;
}

uint16_t get_adc_lm35()
{
  uint16_t adcr;

  // select channel 0 and 1.1V internal reference voltage
  ADMUX = 0b11000000;
  
  // warm up the ADC, discard the first conversion
  ADCSRA |= (1 << ADSC);
  
  while (ADCSRA & (1 << ADSC)) {
    ;
  }

  // start single conversion
  ADCSRA |= (1 << ADSC);
  
  // wait until conversion is done
  while (ADCSRA & (1 << ADSC)) {
    ;
  }

  adcr = ADC; // read out result

  // finally set up hal sensor ADC
  ADMUX = 0b01000001;

  return adcr;
}

// initialize ports
void init()
{
  unsigned long seed;

  DDRA = 0b11111111; // A6:0 are rows 7:1 of display
  PORTA = 0b00000000; // A7 is dout 1
  
  DDRC = 0b11111111; // C6:0 are rows 14:8 of display
  PORTC = 0b00000000; // C7 is ckout 2
  
  DDRD = 0b00000000; // columns 8:1
  PORTD = 0b11111111; // all off

  DDRG = 0b00000; // G4:3 switches, G1:0 columns 10:9
  PORTG = 0b11111; // all off, pullup on switches

  // display interrupt setup
  OCR1A = 0x1F; // number to count up to
  TCCR1A = 0; // Clear Timer on Compare Match (CTC) mode
#if defined ISING_TDD_PPHOELBLING_SIMULATION
  TIMSK = (1 << OCIE1A); // TC0 compare match A interrupt enable
#else
  TIMSK1 = (1 << OCIE1A); // TC0 compare match A interrupt enable
#endif  // defined ISING_TDD_SIMULATION
  TCCR1B = (1 << CS10) | (1 << CS11) | (1 << WGM12); // prescaler to 64, CTC mode

  // get the random seed from EEPROM
  eeprom_read_block((void*)&seed, (const void*)&ee_seed, 4);
  srandom(seed);
  
  // increment and store it to EEPROM
  seed += random();
  eeprom_write_block(&seed, &ee_seed, 4);

  // set original key state
  okeyB7 = ((PING & (1 << 3)) == 0);
  okeyA5 = ((PING & (1 << 2)) == 0);
  
  sei(); // interrupts on

  // ADC setup
  ADCSRA = 0b10000111; // ADC enable and prescaler to 128 
}

// check the toggle BC key
uint8_t checkkeyA5()
{
  uint8_t cs;

  cs = (PING >> 4) & 1;

  // key release event
  if ((cs != tk) && (cs == 1)) {
    tk = cs;
    
    return 1;
  } else { // nothing to report
    tk = cs;
      
    return 0;
  }
}

// check the toggle antifer key
// careful, this is the reset key
uint8_t checkkeyB7()
{
  uint8_t cs;

  cs = (PING >> 3) & 1;
  
  // key release event
  if ((cs != ak) && (cs == 1)) {
    ak = cs;
      
    return 1;
  } else { // nothing to report
    ak = cs;
    
    return 0;
  }
}

// initialize spins
// therm: 0->cold 1->hot
void initspins(uint8_t therm)
{
  uint8_t x, y;
  uint16_t tp = 0b1010101010101010;

  switch(therm) {
    case(INIT_COLD):
      for (x = 0; x < NX; ++x) {
        spin[x] = 0;
      }
      
      break;
    case(INIT_HOT):
      for (x = 0; x < NX; ++x) {
        spin[x] = 0;
        
        for (y = 0; y < NY; ++y) {
          spin[x] |= rnd(2) << (YOFS+y);
        }
      }
      
      break;
    case(INIT_ANTIFER):
      for (x = 0; x < NX; ++x) {
        spin[x] = tp;
        tp = ~tp;
      }
      
      break;
    case(INIT_FULL):
      for (x = 0; x < NX; ++x) {
        spin[x] = ~0;
      }
      
      break;
  }
}

void worker(uint8_t nup)
{
  uint8_t i, x, y, ts, xf, xb;
  int8_t nn;
  float de, ran;
  
  for (i = 0; i < nup; ++i) {
    x = rnd(NX);
    y = rnd(NY);
    xf = (x+1) % NX;
    xb = (x+NX-1) % NX;
    
    // copy boundaries in y direction, optionally antiperiodic
    if (y == 0) {
      spin[x] = (spin[x]&(~(1<<(YOFS-1)))) |
                ((((spin[x]>>(YOFS+NY-1))&1)^bc)<<(YOFS-1));
    }
      
    if (y == NY-1) {
      spin[x] = (spin[x] & (~(1 << (YOFS+NY)))) |
                ((((spin[x] >> YOFS) & 1)^bc) << (YOFS+NY));
    }
      
    // the spin we are investigating
    ts = (spin[x] >> (YOFS+y)) & 1;
    
    // its alignment with the x-neighbors
    nn = ((ts^(spin[x] >> (YOFS+y-1))) & 1) +
         ((ts^(spin[x] >> (YOFS+y+1))) & 1);
    
    // and the y-neighbors
    nn += ((ts^(spin[xf] >> (YOFS+y))) & 1) +
          ((ts^(spin[xb] >> (YOFS+y))) & 1);
    
    // compute enegy difference
    de = (float)((nn << 2)-8)*beta + (float)(2-(ts << 2))*h;
    
    // and do the MC step
    ran = (float)(random())/(float)(RANDOM_MAX);
    if (exp(de)>ran) {
      spin[x] ^= 1 << (YOFS+y);
    }
  }
}

namespace ising_tdd_hal {

/**
 * @brief Represents the custom PCB designed by prof. Hoelbling. Fitted with an
 *        ATmega645.
 */
class Device final : public ising_tdd_hal::AbstractDevice {
 public:
  explicit Device() {}
  ~Device() override {}

  void operator delete(void*, unsigned int) {}

  void Setup() override {
    init();

    initspins(INIT_ANTIFER);
    _delay_ms(200);
    
    beta = (float)(faf)*0.4;
    h = 0.0;
    
    initspins(INIT_HOT);
  }

  void Loop() override {
    // get temperature
    beta=(float)(faf)*BETAC*pow((float)get_adc_lm35()/TMID, TPOW);
        
    worker(70);
  
    // get magnetic field
    if (M0 < 0.0) {
      M0=(float)get_adc_hal();
      numdisp((uint32_t)M0,0);
      _delay_ms(500);
      initspins(INIT_HOT);
    }
      
    h = (M0-(float)get_adc_hal())*HALGAIN;

    worker(70);
      
    // BC toggle
    if (checkkeyB7() > 0) {
      bc = 1-bc;
    }
      
    // fer <-> antifer toggle
    if (checkkeyA5() > 0) {
      faf = -faf;
    }
  }

 private:
  // ising_tdd_state::Lattice<ISING_TDD_HAL_ARDUINO_ST7735_N_ROWS,
  //                          ISING_TDD_HAL_ARDUINO_ST7735_N_COLUMNS> lattice_;

  // uint8_t row_;
  // uint8_t column_;
};

}

#endif  // ISING_TDD_HAL_PP_HOELBLING_HPP_

