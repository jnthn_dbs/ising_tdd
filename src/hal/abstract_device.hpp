#ifndef ISING_TDD_HAL_ABSTRACT_DEVICE_HPP_
#define ISING_TDD_HAL_ABSTRACT_DEVICE_HPP_

#include "algorithm.hpp"

namespace ising_tdd_hal {

/**
 * @brief Base class for hardware platforms.
 *
 * Implementing classes all have the same name %ising_tdd_hal::Device so that
 * exactly one hardware platform must be linked to compile successfully.
 */
class AbstractDevice {
 public:
  AbstractDevice(): algorithm_(.1, 0) {}
  virtual ~AbstractDevice() {}

  void operator delete(void*, unsigned int) {}

  /**
   * @brief Invoked once on startup.
   */
  virtual void Setup() = 0;

  /**
   * @brief Invoked repeatedly, indefinetily.
   */
  virtual void Loop() = 0;

 protected:
  ising_tdd_algorithm::Algorithm algorithm_;
};

}

#endif  // ISING_TDD_HAL_ABSTRACT_DEVICE_HPP_

