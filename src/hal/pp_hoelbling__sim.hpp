#ifndef ISING_TDD_HAL_PP_HOELBLING__SIM_HPP_
#define ISING_TDD_HAL_PP_HOELBLING__SIM_HPP_

#include "abstract_device__sim.hpp"

#include <simavr/avr_ioport.h>

#include <simavr/sim_avr.h>
#include <simavr/sim_elf.h>
#include <simavr/sim_irq.h>
#include <simavr/sim_vcd_file.h>

#include <stdlib.h>

namespace ising_tdd_hal {

/**
 * @brief Simulation based on a Microchip ATmega128, which has the same pinout
 *        as the Microchip ATmega645, the chip on the custom PCB.
 */
class DeviceSimulation final : public ising_tdd_hal::AbstractDeviceSimulation {
 public:
  explicit DeviceSimulation() {}
  ~DeviceSimulation() override {}

  int Simulate() override {
    elf_firmware_t firmware;
    elf_read_firmware("./build/Ising_TDD_PpHoelbling", &firmware);
  
    //avr_t *avr = avr_make_mcu_by_name(firmware.mmcu);
    avr_t *avr = avr_make_mcu_by_name("atmega128");
    avr_init(avr);
    avr_load_firmware(avr, &firmware);
  
    avr_vcd_t vcd_file;
    avr_vcd_init(avr, "./output/pp_hoelbling__sim.vcd", &vcd_file, 1000);
  
    avr_vcd_add_signal(&vcd_file,
                       avr_io_getirq(avr, AVR_IOCTL_IOPORT_GETIRQ('A'), 
                                     IOPORT_IRQ_REG_PORT),
                       8, "PORTA");
  
    avr_vcd_add_signal(&vcd_file,
                       avr_io_getirq(avr, AVR_IOCTL_IOPORT_GETIRQ('C'), 
                                     IOPORT_IRQ_REG_PORT),
                       8, "PORTC");
  
    avr_irq_t *output_handle_a = avr_io_getirq(avr, AVR_IOCTL_IOPORT_GETIRQ('A'),
                                             IOPORT_IRQ_REG_PORT);
 
    avr_irq_t *output_handle_c = avr_io_getirq(avr, AVR_IOCTL_IOPORT_GETIRQ('C'),
                                             IOPORT_IRQ_REG_PORT);
 
    avr_vcd_start(&vcd_file);
  
    avr_irq_register_notify(output_handle_a, OnPortAChanged, NULL);
    avr_irq_register_notify(output_handle_c, OnPortCChanged, NULL);
  
    while (1) {
      int state = avr_run(avr);
  
      if (state == cpu_Done || state == cpu_Crashed) {
        break;
      }
    }
  
    return 1;
  }

 private:
  static int test_counter;
  
  static void OnPortAChanged(avr_irq_t *irq, uint32_t value, void *param) {
    if (++test_counter == 100000) {
      exit(0);
    }
  }
  
  static void OnPortCChanged(avr_irq_t *irq, uint32_t value, void *param) {
    if (++test_counter == 100000) {
      exit(0);
    }
  }
};

}

int ising_tdd_hal::DeviceSimulation::test_counter = 0;

#endif  // ISING_TDD_HAL_ARDUINO_ST7735__SIM_HPP_

