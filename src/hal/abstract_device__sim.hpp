#ifndef ISING_TDD_HAL_ABSTRACT_DEVICE__SIM_HPP_
#define ISING_TDD_HAL_ABSTRACT_DEVICE__SIM_HPP_

namespace ising_tdd_hal {

/**
 * @brief Base class for simulations of an Ising model simulation on a given
 *        hardware platform.
 */
class AbstractDeviceSimulation {
 public:
  AbstractDeviceSimulation() {}
  virtual ~AbstractDeviceSimulation() {}

  virtual int Simulate() = 0;
};

}

#endif  // ISING_TDD_HAL_ABSTRACT_DEVICE__SIM_HPP_

