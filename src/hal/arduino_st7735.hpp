#ifndef ISING_TDD_HAL_ARDUINO_ST7735_HPP_
#define ISING_TDD_HAL_ARDUINO_ST7735_HPP_

#define ISING_TDD_HAL_ARDUINO_ST7735_N_ROWS 120
#define ISING_TDD_HAL_ARDUINO_ST7735_N_COLUMNS 110

#include "abstract_device.hpp"
#include "lattice.hpp"
#include "spin.hpp"

#include <SPI.h>
#include <TFT.h>

namespace ising_tdd_hal {

/**
 * @brief Represents an Arduino Uno, fitted with an external TFT-display with an
 *        ST7735-based driver.
 */
class Device final : public ising_tdd_hal::AbstractDevice {
 public:
  explicit Device(): tft_screen_(10, 9, 8) {}
  ~Device() override {}

  void operator delete(void*, unsigned int) {}

  void Setup() override {
    algorithm_.SetInverseTemperature(0.442);

    lattice_.InitHot();

    tft_screen_.begin();
    tft_screen_.background(255, 255, 255);
  }

  void Loop() override {
    row_ = random(ISING_TDD_HAL_ARDUINO_ST7735_N_ROWS);
    column_ = random(ISING_TDD_HAL_ARDUINO_ST7735_N_COLUMNS);

    /**
     *  +---+
     *  | X |
     *  +---+
     */
    if (row_ > 0 &&
        row_ < ISING_TDD_HAL_ARDUINO_ST7735_N_ROWS - 1 &&
        column_ > 0 &&
        column_ < ISING_TDD_HAL_ARDUINO_ST7735_N_COLUMNS - 1) {
      algorithm_.CalculateSpin(lattice_[row_][column_],
                               lattice_[row_ - 1][column_],
                               lattice_[row_][column_ + 1],
                               lattice_[row_ + 1][column_],
                               lattice_[row_][column_ - 1]);
    /**
     *  +-X-+
     *  |   |
     *  +---+
     */
    } else if (row_ == 0 &&
               column_ > 0 &&
               column_ < ISING_TDD_HAL_ARDUINO_ST7735_N_COLUMNS - 1) {
      algorithm_.CalculateSpin(lattice_[row_][column_],
                               lattice_[row_][column_ + 1],
                               lattice_[row_ + 1][column_],
                               lattice_[row_][column_ - 1]);
    /**
     *  +---+
     *  |   X
     *  +---+
     */
    } else if (row_ > 0 &&
               row_ < ISING_TDD_HAL_ARDUINO_ST7735_N_ROWS - 1 &&
               column_ == ISING_TDD_HAL_ARDUINO_ST7735_N_COLUMNS - 1) {
      algorithm_.CalculateSpin(lattice_[row_][column_],
                               lattice_[row_ - 1][column_],
                               lattice_[row_ + 1][column_],
                               lattice_[row_][column_ - 1]);
    /**
     *  +---+
     *  |   |
     *  +-X-+
     */
    } else if (row_ == ISING_TDD_HAL_ARDUINO_ST7735_N_ROWS - 1 &&
               column_ > 0 &&
               column_ < ISING_TDD_HAL_ARDUINO_ST7735_N_COLUMNS - 1) {
      algorithm_.CalculateSpin(lattice_[row_][column_],
                               lattice_[row_ - 1][column_],
                               lattice_[row_][column_ + 1],
                               lattice_[row_][column_ - 1]);
    /**
     *  +---+
     *  X   |
     *  +---+
     */
    } else if (row_ > 0 &&
               row_ < ISING_TDD_HAL_ARDUINO_ST7735_N_ROWS - 1 &&
               column_ == 0) {
      algorithm_.CalculateSpin(lattice_[row_][column_],
                               lattice_[row_ - 1][column_],
                               lattice_[row_][column_ + 1],
                               lattice_[row_ + 1][column_]);
    /**
     *  +---X
     *  |   |
     *  +---+
     */
    } else if (row_ == 0 &&
               column_ == ISING_TDD_HAL_ARDUINO_ST7735_N_COLUMNS - 1) {
      algorithm_.CalculateSpin(lattice_[row_][column_],
                               lattice_[row_ + 1][column_],
                               lattice_[row_][column_ - 1]);
    /**
     *  +---+
     *  |   |
     *  +---X
     */
    } else if (row_ == ISING_TDD_HAL_ARDUINO_ST7735_N_ROWS - 1 &&
               column_ == ISING_TDD_HAL_ARDUINO_ST7735_N_COLUMNS - 1) {
      algorithm_.CalculateSpin(lattice_[row_][column_],
                               lattice_[row_ - 1][column_],
                               lattice_[row_][column_ - 1]);
    /**
     *  +---+
     *  |   |
     *  X---+
     */
    } else if (row_ == ISING_TDD_HAL_ARDUINO_ST7735_N_ROWS - 1 &&
               column_ == 0) {
      algorithm_.CalculateSpin(lattice_[row_][column_],
                               lattice_[row_ - 1][column_],
                               lattice_[row_][column_ + 1]);
    /**
     *  X---+
     *  |   |
     *  +---+
     */
    } else if (row_ == 0 && column_ == 0) {
      algorithm_.CalculateSpin(lattice_[row_][column_],
                               lattice_[row_][column_ + 1],
                               lattice_[row_ + 1][column_]);
    }

    if (lattice_[row_][column_] == ising_tdd_state::SpinValue::kSpinUp) {
      tft_screen_.stroke(255, 0, 0);
    } else {
      tft_screen_.stroke(0, 0, 255);
    }

    tft_screen_.point(20 + row_, 9 + column_);
  }

 private:
  ising_tdd_state::Lattice<ISING_TDD_HAL_ARDUINO_ST7735_N_ROWS,
                           ISING_TDD_HAL_ARDUINO_ST7735_N_COLUMNS> lattice_;

  uint8_t row_;
  uint8_t column_;

  TFT tft_screen_;
};

}

#endif  // ISING_TDD_HAL_ARDUINO_ST7735_HPP_

