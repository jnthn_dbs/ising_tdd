#ifndef ISING_TDD_BOARD
#error "Must set ISING_TDD_BOARD compile option"
#endif  // ISING_TDD_BOARD

#define ISING_TDD_BOARD_ARDUINO 1
#define ISING_TDD_BOARD_PPHOELBLING 2
#define ISING_TDD_BOARD_NONE 3

#if defined ISING_TDD_TESTING
#include <gtest/gtest.h>
#elif defined ISING_TDD_SIMULATION
#if ISING_TDD_BOARD == ISING_TDD_BOARD_ARDUINO
#include "arduino_st7735__sim.hpp"
#elif ISING_TDD_BOARD == ISING_TDD_BOARD_PPHOELBLING
#include "pp_hoelbling__sim.hpp"
#endif  // ISING_TDD_BOARD == ISING_TDD_BOARD_ARDUINO
#else
#if ISING_TDD_BOARD == ISING_TDD_BOARD_ARDUINO
#include "arduino_st7735.hpp"
#elif ISING_TDD_BOARD == ISING_TDD_BOARD_PPHOELBLING
#include "pp_hoelbling.hpp"
#endif  // ISING_TDD_BOARD == ISING_TDD_BOARD_ARDUINO
#endif

#if ISING_TDD_BOARD != ISING_TDD_BOARD_ARDUINO || defined ISING_TDD_SIMULATION
int main() {
#if defined ISING_TDD_TESTING
  testing::InitGoogleTest();
 
  return RUN_ALL_TESTS();
#elif defined ISING_TDD_SIMULATION
  ising_tdd_hal::DeviceSimulation device_simulation;

  return device_simulation.Simulate();
#else
  ising_tdd_hal::Device device;

  device.Setup();

  while (1) {
    device.Loop();
  }

  return 0;
#endif
}
#else
ising_tdd_hal::Device device;

void setup() {
  device.Setup();
}

void loop() {
  device.Loop();
}
#endif  // ISING_TDD_BOARD != ISING_TDD_BOARD_ARDUINO && !defined ISING_TDD_SIMULATION

