#include "spin.hpp"

#include <stdint.h>

#include <gtest/gtest.h>

/**
 * @brief Test whether the bits of a byte can be correctly addressed and
 *        interpreted as spins.
 */
TEST(IsingTddSpinTests, ConstructorAndValueGetter) {
  uint8_t test_bits = 0b11001001;

  ising_tdd_state::Spin test_spin_0(&test_bits, 0);
  ising_tdd_state::Spin test_spin_1(&test_bits, 1);
  ising_tdd_state::Spin test_spin_2(&test_bits, 2);
  ising_tdd_state::Spin test_spin_3(&test_bits, 3);
  ising_tdd_state::Spin test_spin_4(&test_bits, 4);
  ising_tdd_state::Spin test_spin_5(&test_bits, 5);
  ising_tdd_state::Spin test_spin_6(&test_bits, 6);
  ising_tdd_state::Spin test_spin_7(&test_bits, 7);

  ASSERT_EQ(test_spin_0.GetValue(), ising_tdd_state::SpinValue::kSpinUp);
  ASSERT_EQ(test_spin_1.GetValue(), ising_tdd_state::SpinValue::kSpinDown);
  ASSERT_EQ(test_spin_2.GetValue(), ising_tdd_state::SpinValue::kSpinDown);
  ASSERT_EQ(test_spin_3.GetValue(), ising_tdd_state::SpinValue::kSpinUp);
  ASSERT_EQ(test_spin_4.GetValue(), ising_tdd_state::SpinValue::kSpinDown);
  ASSERT_EQ(test_spin_5.GetValue(), ising_tdd_state::SpinValue::kSpinDown);
  ASSERT_EQ(test_spin_6.GetValue(), ising_tdd_state::SpinValue::kSpinUp);
  ASSERT_EQ(test_spin_7.GetValue(), ising_tdd_state::SpinValue::kSpinUp);
}

/**
 * @brief Test whether spin values and the underlying bits can be manipulated.
 */
TEST(IsingTddSpinTests, AssignmentOperators) {
  uint8_t test_bits = 0;

  ising_tdd_state::Spin test_spin_0(&test_bits, 0);
  ising_tdd_state::Spin test_spin_1(&test_bits, 1);

  test_spin_0 = ising_tdd_state::SpinValue::kSpinUp;

  ASSERT_EQ(test_spin_0.GetValue(), ising_tdd_state::SpinValue::kSpinUp);
  ASSERT_EQ(test_spin_1.GetValue(), ising_tdd_state::SpinValue::kSpinDown);
  ASSERT_EQ(test_bits, 0b00000001);

  test_spin_1 = test_spin_0;

  ASSERT_EQ(test_spin_0.GetValue(), ising_tdd_state::SpinValue::kSpinUp);
  ASSERT_EQ(test_spin_1.GetValue(), ising_tdd_state::SpinValue::kSpinUp);
  ASSERT_EQ(test_bits, 0b00000011);
}

/**
 * @brief Test comparison of spin values.
 */
TEST(IsingTddSpinTests, ComparisonOperators) {
  uint8_t test_bits = 0b00000011;

  ising_tdd_state::Spin test_spin_0(&test_bits, 0);
  ising_tdd_state::Spin test_spin_1(&test_bits, 1);
  ising_tdd_state::Spin test_spin_2(&test_bits, 2);
  ising_tdd_state::Spin test_spin_3(&test_bits, 3);

  ASSERT_EQ(test_spin_0 == ising_tdd_state::SpinValue::kSpinUp, true);
  ASSERT_EQ(test_spin_0 != ising_tdd_state::SpinValue::kSpinUp, false);
  ASSERT_EQ(test_spin_0 == ising_tdd_state::SpinValue::kSpinDown, false);
  ASSERT_EQ(test_spin_0 != ising_tdd_state::SpinValue::kSpinDown, true);

  ASSERT_EQ(test_spin_0 == test_spin_1, true);
  ASSERT_EQ(test_spin_0 != test_spin_1, false);
  ASSERT_EQ(test_spin_0 == test_spin_2, false);
  ASSERT_EQ(test_spin_0 != test_spin_2, true);

  ASSERT_EQ(test_spin_2 == ising_tdd_state::SpinValue::kSpinUp, false);
  ASSERT_EQ(test_spin_2 != ising_tdd_state::SpinValue::kSpinUp, true);
  ASSERT_EQ(test_spin_2 == ising_tdd_state::SpinValue::kSpinDown, true);
  ASSERT_EQ(test_spin_2 != ising_tdd_state::SpinValue::kSpinDown, false);

  ASSERT_EQ(test_spin_2 == test_spin_3, true);
  ASSERT_EQ(test_spin_2 != test_spin_3, false);
  ASSERT_EQ(test_spin_2 == test_spin_1, false);
  ASSERT_EQ(test_spin_2 != test_spin_1, true);
}

/**
 * @brief Test manipulation of spin values via the
 *        @ref ising_tdd_state::Spin::Flip method.
 */
TEST(IsingTddSpinTests, Flip) {
  uint8_t test_bits = 0;

  ising_tdd_state::Spin test_spin(&test_bits, 0);
  
  test_spin.Flip();

  ASSERT_EQ(test_spin == ising_tdd_state::SpinValue::kSpinUp == true, true);
  ASSERT_EQ(test_bits, 1);

  test_spin.Flip();

  ASSERT_EQ(test_spin == ising_tdd_state::SpinValue::kSpinDown == true, true);
  ASSERT_EQ(test_bits, 0);
}

