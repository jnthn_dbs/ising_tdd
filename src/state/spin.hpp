#ifndef ISING_TDD_STATE_SPIN_HPP_
#define ISING_TDD_STATE_SPIN_HPP_

namespace ising_tdd_state {

/**
 * @brief %Spin down or up.
 */
enum class SpinValue : unsigned char {
  kSpinDown,
  kSpinUp
};

/**
 * @brief %Spin quantum number.
 *
 * Can take one of either values down or up. See ising_tdd_state::SpinValue.
 * Value type for one-dimensional instances of @ref ising_tdd_state::Lattice and
 * @ref ising_tdd_state::SubLattice.
 *
 * Each spin is represented by a single bit. The bit is addressed via a byte
 * memory address and the number of the bit within that byte. See
 * @ref ising_tdd_state::Spin::kSpinBufferAddress and
 * @ref ising_tdd_state::Spin::kSpinBufferOffset. The bit address must be passed
 * to the constructor on instance creation and cannot be changed afterwards. The
 * underlying memory address space is usually owned by an instance of
 * @ref ising_tdd_state::Lattice.
 *
 * Methods for comparison (see @ref ising_tdd_state::Spin::operator== and
 * @ref ising_tdd_state::Spin::operator!=) and mutation (see
 * @ref ising_tdd_state::Spin::operator= and @ref ising_tdd_state::Spin::Flip)
 * are provided. They use easy, value typed (see
 * @ref ising_tdd_state::SpinValue) interfaces, the underlying memory structures
 * remain transparent after object construction. Mutations will only affect the
 * **value** of the controlled bit and will never change the address given on
 * instance creation.
 */
class Spin final {
 public:
  /**
   * @brief Initializes @ref ising_tdd_state::Spin::kSpinBufferAddress and
   *        @ref ising_tdd_state::Spin::kSpinBufferOffset as the given
   *        parameters and ising_tdd_state::Spin::value_ according to the value
   *        under the bit address.
   *
   * Usually, there is no need to employ this constructor in userland code.
   * Objects of this type are dynamically created by the
   * @ref ising_tdd_state::Lattice::operator[] and
   * @ref ising_tdd_state::SubLattice::operator[] methods of one-dimensional
   * (sub-)lattices.
   */
  explicit Spin(unsigned char *spin_buffer_address,
                unsigned char spin_buffer_offset):
      kSpinBufferAddress(spin_buffer_address),
      kSpinBufferOffset(spin_buffer_offset) {
        value_ = *spin_buffer_address >> spin_buffer_offset & 1
                 ? SpinValue::kSpinUp : SpinValue::kSpinDown;
      }
  
  void operator=(SpinValue value) {
    if (value_ != value) {
      *kSpinBufferAddress ^= 1 << kSpinBufferOffset;
      value_ = value;
    }
  }
  
  void operator=(Spin spin) {
    operator=(spin.GetValue());
  }

  bool operator==(SpinValue value) const {
    return value_ == value;
  }

  bool operator==(Spin spin) const {
    return value_ == spin.GetValue();
  } 

  bool operator!=(SpinValue value) const {
    return value_ != value;
  }

  bool operator!=(Spin spin) const {
    return value_ != spin.GetValue();
  } 

  SpinValue GetValue() const {
    return value_;
  }

  /**
   * @brief Invert the spin value.
   *
   * %Spin down becomes spin up, spin up becomes spin down.
   */
  void Flip() {
    operator=(value_ == ising_tdd_state::SpinValue::kSpinUp ?
              ising_tdd_state::SpinValue::kSpinDown :
              ising_tdd_state::SpinValue::kSpinUp);
  }

 private:
  unsigned char * const kSpinBufferAddress;
  const unsigned char kSpinBufferOffset;

  /**
   * @brief Value type representation of the managed bit.
   *
   * 0 is translated to @ref ising_tdd_state::SpinValue::kSpinDown, 1 is
   * translated to @ref ising_tdd_state::SpinValue::kSpinUp.
   */
  SpinValue value_;
};

}

#endif  // ISING_TDD_STATE_SPIN_HPP_

