#ifndef ISING_TDD_STATE_LATTICE_HPP_
#define ISING_TDD_STATE_LATTICE_HPP_

#include "spin.hpp"

#if ISING_TDD_BOARD != ISING_TDD_BOARD_ARDUINO
#include <stdint.h>
#include <stdlib.h>
#endif  // ISING_TDD_BOARD == ISING_TDD_BOARD_ARDUINO

namespace ising_tdd_state {

#ifdef ISING_TDD_COMPILER_SUPPORTS_CPP20
/**
 * @brief Make sure a lattice never has zero objects along a given dimension.
 *
 * Only works on platforms supporting C++20.
 */
template<uint16_t n>
concept GreaterZero = n > 0;
#endif  // ISING_TDD_COMPILER_SUPPORTS_CPP20

/**
 * @brief (N - 1)-dimensional projection of an N-dimensional lattice or
 *        sublattice.
 *
 * Addresses a subset of the bits (spins) owned by an
 * @ref ising_tdd_state::Lattice (see @ref ising_tdd_state::Lattice::data_) via
 *   - address and offset of the first bit in the subset,
 *   - distance between bits in the subset in the context of its superset and
 *   - number of bits total in the subset.
 */
template<uint8_t dimension, uint8_t ... dimensions>
#ifdef ISING_TDD_COMPILER_SUPPORTS_CPP20
  requires GreaterZero<(dimension * ... * dimensions)>
#endif  // ISING_TDD_COMPILER_SUPPORTS_CPP20
class SubLattice final {
 public:
  /**
   * @brief Establishes the (immutable) addresses of a subset of bits (spins).
   *
   * Usually, there is no need to employ this constructor in userland code.
   * Objects of this type are dynamically created by the
   * @ref ising_tdd_state::Lattice::operator[] and
   * @ref ising_tdd_state::SubLattice::operator[] methods of one-dimensional
   * (sub-)lattices.
   */
  SubLattice(uint8_t *data, const uint8_t buffer_first_spin_offset,
             const uint16_t buffer_size, 
             const uint16_t buffer_step_size):
      data_(data), kBufferFirstSpinOffset(buffer_first_spin_offset), 
      kBufferSize(buffer_size), kBufferStepSize(buffer_step_size) {}

  auto operator[](uint8_t index) {
    if (index >= kBufferSize) {
      index = kBufferSize - 1;
    }

    if constexpr (sizeof...(dimensions) == 0) {
      return ising_tdd_state::Spin(
          &data_[(kBufferFirstSpinOffset + kBufferStepSize * index) / 8],
          (kBufferFirstSpinOffset + kBufferStepSize * index) % 8);
    } else {
      const uint16_t address_base =
          kBufferFirstSpinOffset + 
          kBufferStepSize * index * (1 * ... * dimensions);

      return ising_tdd_state::SubLattice<dimensions ...>(
          &data_[address_base / 8], address_base % 8, (1 * ... * dimensions), 1);
    }
  }

 private:
  uint8_t *data_;

  const uint8_t kBufferFirstSpinOffset;
  const uint16_t kBufferSize;
  const uint16_t kBufferStepSize;
};

/**
 * @brief N-dimensional lattice of objects carrying spin.
 *
 * The number of lattice points per dimension must be explicitly stated at
 * template instantiation each and must never equal zero (on platforms
 * supporting C++20, this is asserted at compile time). The dimensions are split
 * into a template parameter for the first dimension and a parameter pack for
 * the remaining dimensions to allow for recursion.
 *
 * Owns the memory space referenced by objects of types
 * @ref ising_tdd_state::SubLattice and @ref ising_tdd_state::Spin.
 *
 * Dynamically creates projections to (N - 1)-dimensions via
 * @ref ising_tdd_state::Lattice::operator[].
 */
template<uint8_t dimension, uint8_t ... dimensions>
#ifdef ISING_TDD_COMPILER_SUPPORTS_CPP20
  requires GreaterZero<(dimension * ... * dimensions)>
#endif  // ISING_TDD_COMPILER_SUPPORTS_CPP20
class Lattice final {
 public:
  explicit Lattice() {}
  ~Lattice() {} 

  /**
   * @brief Initialize lattice with all spins down.
   */
  void InitCold() {
    for (uint16_t i = 0; i < (dimension * ... * dimensions) / 8 + 1; ++i) {
      data_[i] = 0;
    }
  }

  /**
   * @brief Initialize lattice with each spin randomly down or up.
   */
  void InitHot(unsigned int seed = 1708) {
#if ISING_TDD_BOARD != ISING_TDD_BOARD_ARDUINO
    srand(seed);
#else
    randomSeed(seed);
#endif  // ISING_TDD_BOARD != ISING_TDD_BOARD_ARDUINO
 
    for (uint16_t i = 0; i < (dimension * ... * dimensions) / 8 + 1; ++i) {
#if ISING_TDD_BOARD != ISING_TDD_BOARD_ARDUINO
      data_[i] = rand() % 256;
#else
      data_[i] = random(256);
#endif  // ISING_TDD_BOARD != ISING_TDD_BOARD_ARDUINO
    }
  }
 
  auto operator[](uint8_t index) {
    if constexpr (sizeof...(dimensions) == 0) {
      return ising_tdd_state::Spin(&data_[index / 8], index % 8);
    } else {
      return ising_tdd_state::SubLattice<dimensions ...>(
          &data_[index * (1 * ... * dimensions) / 8],
          index * (1 * ... * dimensions) % 8, (1 * ... * dimensions), 1);
    }
  }

 private:
  uint8_t data_[(dimension * ... * dimensions) / 8 + 1];
};

}

#endif  // ISING_TDD_STATE_LATTICE_HPP_

