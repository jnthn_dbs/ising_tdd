#include "lattice.hpp"
#include "spin.hpp"

#include <stdint.h>
#include <stdlib.h>

#include <gtest/gtest.h>

/**
 * @brief Check for correct type and value of a one-dimensional lattice.
 *
 * Since the lattice is one-dimensional, its values must be of type
 * @ref ising_tdd_state::Spin.
 */
TEST(IsingTddStateLatticeTests, OneDimemsionalLatticeGettersSetters) {
  ising_tdd_state::Lattice<1> lattice;

  ASSERT_EQ(typeid(lattice[0]), typeid(ising_tdd_state::Spin));

  lattice[0] = ising_tdd_state::SpinValue::kSpinUp;

  ASSERT_EQ(lattice[0] == ising_tdd_state::SpinValue::kSpinUp, true);
}

/**
 * @brief Test whether all values of a one-dimensional, cold-initialized lattice
 *        actually are spin down.
 */
TEST(IsingTddStateLatticeTests, OneDimensionalLatticeInitCold) {
  ising_tdd_state::Lattice<3> lattice;

  lattice.InitCold();

  ASSERT_EQ(lattice[0] == ising_tdd_state::SpinValue::kSpinDown, true);
  ASSERT_EQ(lattice[1] == ising_tdd_state::SpinValue::kSpinDown, true);
  ASSERT_EQ(lattice[2] == ising_tdd_state::SpinValue::kSpinDown, true);
}

/**
 * @brief @anchor test_one_dimensional_lattice_init_hot Test whether for a given
 *        random number all bits 0 are translated to spin down and bits 1 to
 *        spin up upon hot-initialization of a lattice.
 */
TEST(IsingTddStateLatticeTests, OneDimensionalLatticeInitHot) {
  ising_tdd_state::Lattice<128> lattice;

  lattice.InitHot(17082019);

  srand(17082019);

  for (uint8_t i = 0; i < 16; ++i) {
    uint8_t random_byte = rand() % 256;

    for (uint8_t j = 0; j < 8; ++j) {
      if (random_byte >> j & 1) {
        ASSERT_EQ(lattice[8 * i + j] == ising_tdd_state::SpinValue::kSpinUp,
                  true);
      } else {
        ASSERT_EQ(lattice[8 * i + j] == ising_tdd_state::SpinValue::kSpinDown,
                  true);
      }
    }
  }
}

/**
 * @brief @anchor test_two_dimensional_lattice_getters_setters Check for correct
 *        types and values of a two-dimensional lattice.
 *
 * A two-dimensional lattice must return values of type
 * @ref ising_tdd_state::SubLattice, the sublattices must return values of type
 * @ref ising_tdd_state::Spin.
 */
TEST(IsingTddStateLatticeTests, TwoDimensionalLatticeGettersSetters) {
  ising_tdd_state::Lattice<2, 2> lattice;
  
  ASSERT_EQ(typeid(lattice[0]), typeid(ising_tdd_state::SubLattice<2>));
  ASSERT_EQ(typeid(lattice[0][0]), typeid(ising_tdd_state::Spin));

  lattice[0][0] = ising_tdd_state::SpinValue::kSpinDown;
  lattice[0][1] = ising_tdd_state::SpinValue::kSpinUp;
  lattice[1][0] = ising_tdd_state::SpinValue::kSpinUp;
  lattice[1][1] = ising_tdd_state::SpinValue::kSpinDown;

  ASSERT_EQ(lattice[0][0] == ising_tdd_state::SpinValue::kSpinDown, true);
  ASSERT_EQ(lattice[0][1] == ising_tdd_state::SpinValue::kSpinUp, true);
  ASSERT_EQ(lattice[1][0] == ising_tdd_state::SpinValue::kSpinUp, true);
  ASSERT_EQ(lattice[1][1] == ising_tdd_state::SpinValue::kSpinDown, true);
}

/**
 * @brief @anchor test_two_dimensional_lattice_init_hot Repeat
 *        @ref test_one_dimensional_lattice_init_hot
 *        "TEST(IsingTddStateLatticeTests, OneDimensinalLatticeInitHot)" with
 *        two dimensions.
 */
TEST(IsingTddStateLatticeTests, TwoDimensionalLatticeInitHot) {
  ising_tdd_state::Lattice<10, 8> lattice;

  lattice.InitHot(17082019);

  srand(17082019);

  for (uint8_t i = 0; i < 10; ++i) {
    uint8_t random_byte = rand() % 256;

    for (uint8_t j = 0; j < 8; ++j) {
      if (random_byte >> j & 1) {
        ASSERT_EQ(lattice[i][j] == ising_tdd_state::SpinValue::kSpinUp, true);
      } else {
        ASSERT_EQ(lattice[i][j] == ising_tdd_state::SpinValue::kSpinDown, true);
      }
    }
  }
}

/**
 * @brief @anchor test_three_dimensional_lattice_getters_setters Repeat
 *        @ref test_two_dimensional_lattice_getters_setters
 *        "TEST(IsingTddStateLatticeTests, TwoDimensionalLatticeGettersSetters)"
 *        with three dimensions.
 */
TEST(IsingTddStateLatticeTests, ThreeDimensionalLatticeGettersSetters) {
  ising_tdd_state::Lattice<2, 2, 2> lattice;
  
  ASSERT_EQ(typeid(lattice[0]), typeid(ising_tdd_state::SubLattice<2, 2>));
  ASSERT_EQ(typeid(lattice[0][0]), typeid(ising_tdd_state::SubLattice<2>));
  ASSERT_EQ(typeid(lattice[0][0][0]), typeid(ising_tdd_state::Spin));

  lattice[0][0][0] = ising_tdd_state::SpinValue::kSpinDown;
  lattice[1][1][1] = ising_tdd_state::SpinValue::kSpinDown;

  ASSERT_EQ(lattice[0][0][0] == ising_tdd_state::SpinValue::kSpinDown, true);
  ASSERT_EQ(lattice[1][1][1] == ising_tdd_state::SpinValue::kSpinDown, true);
}

/**
 * @brief @anchor test_three_dimensional_lattice_init_hot Repeat
 *        @ref test_two_dimensional_lattice_init_hot
 *        "TEST(IsingTddStateLatticeTests, TwoDimensionalLatticeInitHot)" with
 *        three dimensions.
 */
TEST(IsingTddStateLatticeTests, ThreeDimensionalLatticeInitHot) {
  ising_tdd_state::Lattice<10, 10, 8> lattice;

  lattice.InitHot(17082019);

  srand(17082019);

  for (uint8_t i = 0; i < 10; ++i) {
    for (uint8_t j = 0; j < 10; ++j) {
      uint8_t random_byte = rand() % 256;

      for (uint8_t k = 0; k < 8; ++k) {
        if (random_byte >> k & 1) {
          ASSERT_EQ(lattice[i][j][k] == ising_tdd_state::SpinValue::kSpinUp,
                    true);
        } else {
          ASSERT_EQ(lattice[i][j][k] == ising_tdd_state::SpinValue::kSpinDown,
                    true);
        }
      }
    }
  }
}

/**
 * @brief Repeat @ref test_three_dimensional_lattice_getters_setters
 *        "TEST(IsingTddStateLatticeTests, ThreeDimensionalLatticeGettersSetters)"
 *        starting from a sublattice.
 */
TEST(IsingTddStateLatticeTests, ThreeDimensionalSubLatticeGetters) {
  uint8_t test_data[3];

  test_data[0] = 0b10101010;
  test_data[1] = 0b01010101;
  test_data[2] = 0b11001100;

  ising_tdd_state::SubLattice<2, 2, 6> sub_lattice(test_data, 0, 24, 1);
  
  ASSERT_EQ(sub_lattice[0][0][0] == ising_tdd_state::SpinValue::kSpinDown, true);
  ASSERT_EQ(sub_lattice[0][0][1] == ising_tdd_state::SpinValue::kSpinUp, true);
  ASSERT_EQ(sub_lattice[0][0][2] == ising_tdd_state::SpinValue::kSpinDown, true);
  ASSERT_EQ(sub_lattice[0][0][3] == ising_tdd_state::SpinValue::kSpinUp, true);
  ASSERT_EQ(sub_lattice[0][0][4] == ising_tdd_state::SpinValue::kSpinDown, true);
  ASSERT_EQ(sub_lattice[0][0][6] == ising_tdd_state::SpinValue::kSpinUp, true);

  ASSERT_EQ(sub_lattice[0][1][0] == ising_tdd_state::SpinValue::kSpinDown, true);
  ASSERT_EQ(sub_lattice[0][1][1] == ising_tdd_state::SpinValue::kSpinUp, true);
  ASSERT_EQ(sub_lattice[0][1][2] == ising_tdd_state::SpinValue::kSpinUp, true);
  ASSERT_EQ(sub_lattice[0][1][3] == ising_tdd_state::SpinValue::kSpinDown, true);
  ASSERT_EQ(sub_lattice[0][1][4] == ising_tdd_state::SpinValue::kSpinUp, true);
  ASSERT_EQ(sub_lattice[0][1][5] == ising_tdd_state::SpinValue::kSpinDown, true);

  ASSERT_EQ(sub_lattice[1][0][0] == ising_tdd_state::SpinValue::kSpinUp, true);
  ASSERT_EQ(sub_lattice[1][0][1] == ising_tdd_state::SpinValue::kSpinDown, true);
  ASSERT_EQ(sub_lattice[1][0][2] == ising_tdd_state::SpinValue::kSpinUp, true);
  ASSERT_EQ(sub_lattice[1][0][3] == ising_tdd_state::SpinValue::kSpinDown, true);
  ASSERT_EQ(sub_lattice[1][0][4] == ising_tdd_state::SpinValue::kSpinDown, true);
  ASSERT_EQ(sub_lattice[1][0][6] == ising_tdd_state::SpinValue::kSpinDown, true);

  ASSERT_EQ(sub_lattice[1][1][0] == ising_tdd_state::SpinValue::kSpinUp, true);
  ASSERT_EQ(sub_lattice[1][1][1] == ising_tdd_state::SpinValue::kSpinUp, true);
  ASSERT_EQ(sub_lattice[1][1][2] == ising_tdd_state::SpinValue::kSpinDown, true);
  ASSERT_EQ(sub_lattice[1][1][3] == ising_tdd_state::SpinValue::kSpinDown, true);
  ASSERT_EQ(sub_lattice[1][1][4] == ising_tdd_state::SpinValue::kSpinUp, true);
  ASSERT_EQ(sub_lattice[1][1][5] == ising_tdd_state::SpinValue::kSpinUp, true);
}

/**
 * @brief Repeat @ref test_three_dimensional_lattice_init_hot
 *        "TEST(IsingTddStateLatticeTests, ThreeDimensionalLatticeInitHot)"
 *        with five dimensions.
 */
TEST(IsingTddStateLatticeTests, FiveDimensionalLatticeInitHot) {
  ising_tdd_state::Lattice<8, 8, 8, 8, 8> lattice;

  lattice.InitHot(17082019);

  srand(17082019);

  for (uint8_t i = 0; i < 8; ++i) {
    for (uint8_t j = 0; j < 8; ++j) {
      for (uint8_t k = 0; k < 8; ++k) {
        for (uint8_t l = 0; l < 8; ++l) {
          uint8_t random_byte = rand() % 256;

          for (uint8_t m = 0; m < 8; ++m) {
            if (random_byte >> m & 1) {
              ASSERT_EQ(
                  lattice[i][j][k][l][m] == 
                      ising_tdd_state::SpinValue::kSpinUp, 
                  true);
            } else {
              ASSERT_EQ(
                  lattice[i][j][k][l][m] == 
                      ising_tdd_state::SpinValue::kSpinDown, 
                  true);
            }
          }
        }
      }
    }
  }
}

